
import os
import sys
import getpass

import mongoengine

import configparser

from pathlib import Path


HOSTNAME = os.uname()[1]


if HOSTNAME[:9] == "applejack":        #to work on the server
    WHICHDB = "localdb"
elif getpass.getuser() == "ebasar":    #to work on applejack home
    WHICHDB = "mongolab"
else:                                #to work on local
    WHICHDB = "mongolab"


BASE_DIR = os.path.dirname(os.path.dirname(__file__))

config = configparser.ConfigParser()

if(WHICHDB == "localdb"):
    my_file_str = "data/localdb.ini"
    my_file = Path(my_file_str)
    if my_file.is_file():
            # file exists
        config.read(my_file_str)
    else:
        print("The config file does not exists. It should be in the path:"+os.getcwd()+"/"+my_file_str)
        sys.exit()

elif(WHICHDB == "mongolab"):
    config.read("data/ebasar_rel.ini")

else:
    print("The ini file that system tries does not exist")
    print('Or you should configure path of the ini file based on the platform you are using properly.')
    sys.exit()


SECRET_KEY = config.get('rel_settings', 'secret_key')


if HOSTNAME[:9] == "applejack":        #to work on the server
    DEBUG = False
elif getpass.getuser() == "ebasar":    #to work on applejack home
    DEBUG = False
else:                                #to work on local
    DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'relevancer.science.ru.nl']


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mongoengine.django.mongo_auth',
    'main',
    'dashboardapp',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'RelevancerDjango.urls'

WSGI_APPLICATION = 'RelevancerDjango.wsgi.application'



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy',
    },
}

AUTHENTICATION_BACKENDS = (
    'mongoengine.django.auth.MongoEngineBackend',
)

SESSION_ENGINE = 'mongoengine.django.sessions'
SESSION_SERIALIZER = 'mongoengine.django.sessions.BSONSerializer'


AUTH_USER_MODEL = 'mongo_auth.MongoUser'
MONGOENGINE_USER_DOCUMENT = 'mongoengine.django.auth.User'


db_name = config.get('rel_mongo_db', 'db_name')
db_host = config.get('rel_mongo_db', 'client_host')
db_port = int(config.get('rel_mongo_db', 'client_port'))

if(WHICHDB == "localdb"):
    mongoengine.connect(db_name, host=db_host, port=db_port)

elif(WHICHDB == "mongolab"):
    db_uname = config.get('rel_mongo_db', 'user_name')
    db_passwd = config.get('rel_mongo_db', 'passwd')
    mongoengine.connect(db_name, host=db_host, port=db_port, username=db_uname , password=db_passwd)


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Amsterdam'

USE_I18N = True

USE_L10N = True

USE_TZ = True


STATIC_URL = '/static/'


STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    os.path.join(BASE_DIR, "dashboardapp/static"),
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
    os.path.join(BASE_DIR, "dashboardapp/templates"),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')


# Create the necessary custom folders
os.makedirs("data/backups/"    , exist_ok=True)
os.makedirs("data/classifiers/"    , exist_ok=True)


APIURL = 'http://127.0.0.1:5006/'
