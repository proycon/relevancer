import os
import sys
from datetime import datetime, timedelta
import configparser
import time

import requests
import pymongo as pm
import pandas as pd
import numpy as np
import json

config = configparser.ConfigParser()

HOSTNAME = os.uname()[1]

if HOSTNAME[:9] == "applejack":      #to work on the server
    WHICHDB = "localdb"
else:                                #to work on local
    WHICHDB = "mongolab"

print(WHICHDB)

if(WHICHDB == "localdb"):
    config.read("data/localdb.ini")

elif(WHICHDB == "mongolab"):
    config.read("data/ebasar_rel.ini")


config.sections()
print(config.sections())

# MongoDB Connection
connection = pm.MongoClient(config['rel_mongo_db']['client_host'], int(config['rel_mongo_db']['client_port']))    
db = connection[config['rel_mongo_db']['db_name']]

if(WHICHDB == "mongolab"):
    db.authenticate(config['rel_mongo_db']['user_name'], config['rel_mongo_db']['passwd'])
    
collname = 'floodtags20140729_tagalog'

coll = db[collname]

payload = {'apiKey': config['api']['apikey']}
payload['until'] = '2014-12-01T00:00:00.000Z'
payload['since']='2014-07-29T09:46:32.000Z'
payload['hasLocations']='false'

filtersource = 'philippines'
#filtersource = 'flood'


r = requests.get('https://api.floodtags.com/v1/tags/' + filtersource + '/index?', params=payload)

skprange = int(r.json()['meta']['total']/50)

print('Total number of tweets: ' + str(r.json()['meta']['total']) + ', Number of expected loops: ' + str(skprange))

for skp in range(0,skprange):
    payload['skip']=str(skp*50)
    r = requests.get('https://api.floodtags.com/v1/tags/philippines/index?', params=payload)
    try:
        coll.insert_many([t for t in r.json()['tags']])
    except:
        for t in r.json()['tags']:
            coll.insert(t)

    prcnt = (skp*100)/skprange
    if(prcnt%10==0):
         print(str(int(prcnt)) + '% completed')


# Add the collection name to collectionlist and models.py;

CLcoll = db['CollectionList']

collectionlist = CLcoll.find()[0]

if(collname not in collectionlist['collectionlist_unclus']):
    collectionlist['collectionlist_unclus'].append(collname)
    collectionlist['collectionlist_floodtags'].append(collname)

    CLcoll.remove()
    CLcoll.insert(collectionlist)

	with open("main/models.py", "a") as modelfile:
	   			modelfile.write("\nclass " + collname + "(TweetCollFloodTags):\n\n\t meta = {'collection': '" + collname + "'}\n")

