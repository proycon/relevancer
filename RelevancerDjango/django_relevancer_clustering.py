#!../.env/bin/python

# Python
import re
import os
import sys
import json
import requests
import logging
import smtplib
import subprocess
import configparser
import iso8601
from dateutil import parser

# Our Own Sources
sys.path.append('../') # adds 'Relevancer' folder to PYTHONPATH to find relevancer.py etc.
import relevancer as rlv

def send_mail(sbj, msg, to):

    configauth = configparser.ConfigParser()
    configauth.read("data/auth.ini")

    if(to=="admin"):
        toaddrs = configauth.get('mail', 'admin')
    else:
        toaddrs  = to

    #Mail Auth;
    fromaddr = configauth.get('mail', 'fromaddr')
    username = configauth.get('mail', 'user_name')
    password = configauth.get('mail', 'password')
    subject = sbj
    message = 'Subject: ' + subject + '\n\n' + msg
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(username,password)
    server.sendmail(fromaddr, toaddrs, message)
    server.quit()


# Relevancer Clustering;

def auto_cluster(collname, queryid, APIURL):

    logging.basicConfig(
            format='%(asctime)s, %(levelname)s: %(message)s',
            filename='data/relevancer-clustering.log',
            datefmt='%d-%m-%Y, %H:%M',
            level=logging.INFO)

    logging.info('Clustering started')

    query = requests.get(APIURL + 'getdata/relevancer/'+ collname +'/objectid/' + queryid).json()

    useremail = query['useremail']

    filtername = query['filtername']

    sbj = 'Clustering Started - Relevancer'

    msg = '\n\nClustering is started' +\
            '\n\nThe collection name: ' + collname +\
            '\nThe filtername: ' + filtername +\
            '\n\nUser Email : ' + useremail +\
            '\nYou will get another email from us when the data is ready to be annotated.'

    send_mail(sbj, msg, useremail)
    send_mail(sbj, msg, "admin")

    optionnames = ['urls','photos', 'nophotosurls']
#    optionnames = ['nophotosurls']
    clusternamelist = []

    for opt in optionnames:

        tweets = requests.get(APIURL + 'getdata/relevancerdatasets/'+ collname + '/tweetlists/'+ queryid + '/'+ opt).json()['tweetlists']

        if len(tweets) == 0:
            continue
        else:
            active_col = 'active_text'
            rlv.set_active_column(active_col)

            my_token_pattern = r"[-+$€£]?\d+(?:[.,/-]\d+)*%?|\w+(?:[-&]\w+)*|[#@]?\w+\b|[\U00010000-\U0010ffff\U0001F300-\U0001F64F\U0001F680-\U0001F6FF\u2600-\u26FF\u2700-\u27BF]|['-.:()\[\],;?!*]{2,4}"

            rlv_tweets_list = []
            for t in tweets:
                #print(type(t["date"]), iso8601.parse_date(t["date"]))
                #print("tweetsample:",t)
                #break
                if 'date' not in t:
                    #print("the key date was not found in t:", type(t), t)
                    continue
                else:
                    rlv_tweets_list.append({'text': t['text'], 'created_at':parser.parse(t["date"]),'id_str': t['source']['id'], 'user_id': t['source']['user']['id']})
            #continue
            #print(rlv_tweets_list[0])
            #break
            if(len(rlv_tweets_list)>0):

                tweets_df = rlv.create_dataframe(rlv_tweets_list)
                #print(tweets_df[:3])
                #break
                tweets_df[active_col] = tweets_df["text"].copy()

                tweets_df = rlv.tok_results(tweets_df, elimrt=False)

                tweets_df = rlv.normalize_text(tweets_df)

                #cluster_list = rlv.create_clusters(tweets_df, my_token_pattern, user_identifier='user_id')
                cluster_list = []
                for i,group in enumerate(tweets_df.groupby(tweets_df.index.day)):
                    tweets_date = group[1].index[0].date()
                    print(i, tweets_date, len(group[1]), end=", ")
                    day_cluster_list = rlv.create_clusters(group[1], my_token_pattern, min_dist_thres=0.7, max_dist_thres=0.85, min_max_diff_thres=0.4, nameprefix='1-', min_clusters=30, user_identifier='user_id')
                    #to_text_file("20160809_earthq_"+str(tweets_date)+"_clusters50_1..1.txt", day_cluster_list)
                    print("len(day_cluster_list):",len(day_cluster_list))
                    cluster_list += day_cluster_list
                    #break # just for test, remove this line later.
                logging.info("Group based clustering is done")
                #continue
                clustered_ids = []
                #for dt, clst_day in cluster_list_per_day:
                for clst in cluster_list:
                    clustered_ids += clst["twids"]
                #print("clustered_ids:", len(clustered_ids), len(set(clustered_ids)))
                #print(clustered_ids)
                print(len(tweets_df[tweets_df.id_str.isin(clustered_ids)]), len(tweets_df[~tweets_df.id_str.isin(clustered_ids)]))
                cluster_list_all_days = rlv.create_clusters(tweets_df[~tweets_df.id_str.isin(clustered_ids)], my_token_pattern, user_identifier='user_id')
                cluster_list += cluster_list_all_days
                print("len clusters from alldays:", len(cluster_list_all_days))
                #break
                #print(cluster_list[0].keys())
                # Save to database;
                print('final len(cluster_list):',len(cluster_list))

                logging.info("Clustering is done")

                collname_cluster = collname + "_" + filtername + "_" + opt

                clusternamelist.append(collname_cluster)

                savetodb = requests.post(APIURL + 'save/relevancerclusters/'+ collname_cluster + '/cluster', json=json.dumps({'clusterlist':cluster_list}))


                sbj = "Clustered " + opt
                msg = "Clustered " + opt + " for " + collname
                send_mail(sbj, msg, "admin")

            else:
                print('There is not any tweet with a specified date')

    # Send Email to user and admin when all is done

    sbj = "Clustering is Done - Relevancer"

    clusternames = ', \n'.join(clusternamelist)

    msg = '\n\nClustering is done' +\
            '\n\nThe collection name: ' + collname +\
            '\nThe filtername: ' + filtername +\
            '\n\nPlease go to relevancer.science.ru.nl/datasets. The clusters to be annotated are; \n' + clusternames +\
            '\n\nThis mail was sent to : ' + useremail

#    send_mail(sbj, msg, useremail)
    send_mail(sbj, msg, "admin")


if __name__=='__main__':

    APIURL = 'http://127.0.0.1:5006/'

#    collname = 'floodIndonesiaFeb2017'
 #   queryid = '5a5f2936385ee4775a64baee'

    auto_cluster(collname, queryid, APIURL)
